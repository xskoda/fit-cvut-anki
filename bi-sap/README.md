# BI-SAP

## Tagy

Balíček obsahuje nestandarní tagy:

- `bi-sap::otazkyXX` -- kartičky s otázkami z přednáškové prezentace `XX`, které se mohou objevit v testech;
- `bi-sap::testXX` -- kartičky s otázkami z minulých testů.

