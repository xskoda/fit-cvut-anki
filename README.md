# FIT ČVUT Anki kartičky

Do tohoto repozitáře budu postupně přidávat všechny svoje balíčky Anki kartiček vytvořené k předmětům na FIT ČVUT a jejich aktualizace. Níže je popsán postup importování balíčků, významy flagů a tagů. Případné další informace specifické pro konkrétní balíčky se nachází v `README.md` souborech v jednotlivých adresářích.

## Předměty

### 2024/2025 ZS
 - [NI-KOP](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/ni-kop/ni-kop)
 - [NI-MPI](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/ni-mpi/ni-mpi)
 - [NI-REV](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/ni-rev/ni-rev)
 - [NI-SBF](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/ni-sbf/ni-sbf)
 - [NIE-BLO](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/nie-blo/nie-blo)

### 2023/2024 LS
 - [BI-TAB](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-tab/bi-tab)

### 2023/2024 ZS
 - [BI-ASB](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-asb/bi-asb)
 - [BI-HWB](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-hwb/bi-hwb)
 - [BI-PST](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-pst/bi-pst)
 - [BI-ZSB](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-zsb/bi-zsb)

### 2022/2023 LS
 - [BI-ADU](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-adu/bi-adu)
 - [BI-BEK](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-bek/bi-bek)
 - [BI-EHA](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-eha/bi-eha)
 - [BI-KAB](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-kab/bi-kab)
 - [BI-OSY](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-osy/bi-osy)

### 2022/2023 ZS
 - [BI-AAG](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-aag/bi-aag)
 - [BI-AG1](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-ag1/bi-ag1)
 - [BI-APS](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-aps/bi-aps)
 - [BI-MA2](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-ma2/bi-ma2)
 - [BI-UKB](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-ukb/bi-ukb)

### 2021/2022 LS
 - [BI-DBS](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-dbs/bi-dbs)
 - [BI-MA1](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-ma1/bi-ma1)
 - [BI-PA2](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-pa2/bi-pa2)
 - [BI-PSI](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-psi/bi-psi)
 - [BI-SAP](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-sap/bi-sap)

### 2021/2022 ZS
 - [BI-DML](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-dml/bi-dml)
 - [BI-LA1](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-la1/bi-la1)
 - [BI-PA1](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/bi-pa1/bi-pa1)

## Importování

Všechny balíčky jsou exportovány s kompatibilitou pouze pro nové verze Anki (v případě problémů vytvořte issue nebo mě kontaktujte). Zároveň, aby byly v exportech obsaženy flagy a informace o *suspended* kartičkách, obsahují i moje *scheduling infromation*. Balíček se importuje následujícím způsobem:

- V levém horním rohu vybereme *File* / *Import...* (klávesová zkratka <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd>).
- Vybereme soubor s exportovaným balíčkem (přípona `.apkg`).
- Pokud chceme zachovat flagy a *suspended* kartičky, zaškrtneme *Import any learning progress*.
- Stiskneme *Import*.

Pokud se rozhodneme importovat balíček včetně *scheduling information* kvůli flagům a *suspended* kartičkám, můžeme *scheduling information* odstranit a ponechat pouze flagy a informace o *suspended* kartičkách:

- Otevřeme *Browse* (klávesová zkratka <kbd>B</kbd>).
- Vyfiltrujeme pouze kartičky z importovaného balíčku, které nejsou *suspended*.

```plain
deck:NI-EXAMPLE -is:suspended
```

- Označíme všechny vyfiltrované kartičky (klávesová zkratka <kbd>Ctrl</kbd> + <kbd>A</kbd>).
- Resetujeme jejich *scheduling information* kliknutím pravým tlačítkem a vybráním *Reset...* (klávesová zkratka <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>N</kbd>).

> :warning: **Pokud importujete balíčky včetně *scheduling information*, vždy prověďte kontrolu *suspended* kartiček.**

## Flagy

Flagy mají následující významy:

- :red_circle: -- kartička s chybou (faktickou, typografickou nebo jinou) nebo kartička označená pro kontrolu či předělání;
- :orange_circle: -- kartička bude v následující aktualizaci balíčku odstraněna;
- :blue_circle: -- kartička byla v poslední aktualizaci balíčku upravena;
- :green_circle: -- kartička je duplicitní (více níže).

Ostatní flagy jsou bez fixního významu.

## Duplicity

Vzhledem k tomu, že se obsah některých předmětů může překrývat, mohou i některé balíčky obsahovat kopie stejných kartiček. V takovém případě je typicky (ne vždy) aktivní pouze první výskyt a ostatní jsou *suspended* a označeny zeleným flagem. Příkladem je kartička `1677241820085` -- *„What is a threat? Give examples.“*. Ta se vyskytuje v následujících balíčcích:

| Balíček | Podoba | Stav | Flag |
| ------- | ------ | ---- | ---- |
| BI-UKB | „*Definujte hrozbu. Uveďte příklady.*“ | *suspended* | :green_circle: |
| BI-BEK | „*Definujte hrozbu. Uveďte příklady.*“ | *suspended* | :green_circle: |
| BI-EHA  | „*What is a threat? Give examples.*“ | aktivní | - |
| NI-SBF  | „*What is a threat? Give examples.*“ | *suspended* | :green_circle: |

## Tagy

Každý balíček má svůj odpovídající tag (např. `BI-PST` má tag `bi-pst`), v rámci kterého je obsaženo dělení na další tagy. Pokud jej balíček obsahuje, pak má následující význam:

- `pXX` -- kartička pochází z přednáškové prezentace `XX`,
- `cXX` -- kartička pochází ze cvičení `XX`,
- `psXX` -- kartička pochází z prosemináře `XX`.

Kromě předmětových tagů obsahují balíčky i státnicové tagy:

- `bi-spol::XX` -- společné bakalářské okruhy (`BI21`), otázka `BI-SPOL.21-XX`;
- `bi-ib::XX` -- okruhy pro specializaci Informační bezpečnost (`BI-IB.21`), otázka `BI-IB.21-XX`;
- `ni-spol::XX` -- společné magisterské okruhy (`NI`), otázka `NI-SPOL-XX`;
- `ni-pb::XX` -- okruhy pro specializaci Počítačová bezpečnost (`NI-PB`), otázka `NI-PB-XX`.

Případné další tagy jsou dokumentovány v `README.md` jednotlivých předmětů.

## Chyby, návrhy a přispívání

Pro chyby prosím zakládejte issues, nebo pište na můj Discord `ajik`. Při přispívání si prosím aktualizujte svůj adresář pro *hooks* pomocí

```bash
git config core.hooksPath .githooks
```

a používejte *commit messages* ve formátu `[branch] commit message`. Balíčky jsou verzovány pomocí LFS.
