#!/bin/bash

for BRANCH in $(git branch -la | tail -n+2 | tr -s ' ' |cut -d' ' -f2)
do
	COMMIT_MSG="$(git log -1 --pretty=%B $BRANCH -- | head -1)"
	FORMAT="^\[($(git show-ref | grep $(git rev-parse $BRANCH) | cut -d' ' -f2 | rev | cut -d'/' -f1 | rev | tr '\n' '|' | rev | cut --complement -c1 | rev))\] .+"
	
	if [[ "$COMMIT_MSG" =~ $FORMAT ]]
	then
		echo "Latest commit on branch $BRANCH OK!"
	else
		echo "Wrong commit message format on branch $BRANCH!" 1>&2
		echo "message: $COMMIT_MSG" 1>&2
		echo "format: $FORMAT" 1>&2
		exit 1
	fi
done

exit 0
