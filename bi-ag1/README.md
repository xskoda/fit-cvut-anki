# BI-AG1

## Vynechaná témata

Balíček neobsahuje následující témata:

 - všechny důkazy (krom jednoho) věty o charakterizaci stromů ([3:2](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p3-handout.pdf)),
 - všechna lemmata o binární haldě (krom těch o korektnosti algoritmů) ([4](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p4-handout.pdf)),
 - všechna lemmata k binomiálním stromům ([5](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p5-handout.pdf)),
 - důkaz tvrzení o výskytu binomiálního stromu řádu i v binomiální haldě ([5:19](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p5-handout.pdf)),
 - věta o dolním odahu složitostí operací nad BVS a její důkaz ([6:19](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p6-handout.pdf)),
 - implementace slovníků ([7:3](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p7.pdf)),
 - hledání a vkládání prvků hešovací tabulky s otevřenou adresací ([7:17](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p7.pdf)),
 - důaz časové složitosti QuickSelectu s náhodnými vstupy ([8:33](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p8.pdf)),
 - časová složitost rekurzivního algoritmu hledání délky NRP ([10:15](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p10-handout.pdf)),
 - důkaz korektnosti iterativní konstrukce NRP ([10:23](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p10-handout.pdf)),
 - grafový pohled na NRP ([10:24](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p10-handout.pdf)),
 - časová složitost rekurzivního algoritmu hledání EVŘ ([10:31](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p10-handout.pdf)),
 - triangulace konvexního mnohoúhelníku ([10:36](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p10-handout.pdf)),
 - důkaz lemmatu o řezech v grafu s unikátními vahami ([11:11](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p11-handout.pdf)),
 - lemmata k hledání nejkratších cest v grafu ([12](https://courses.fit.cvut.cz/BI-AG1/lectures/media/bi-ag1-p12-handout.pdf)).
