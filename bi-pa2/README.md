# BI-PA2

Balíček není komplentí a obsahuje především kartičky k státnícovým otázkám.

## Tagy

Balíček obsahuje nestandardní tag `bi-pa2::ustni`, který označuje kartičky s otázkami z ústní zkoušky.
